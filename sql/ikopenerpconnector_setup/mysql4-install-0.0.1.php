<?php

/*
 * Cyril's
 */

$installer = $this;
$installer->startSetup();
$db = $installer->getConnection();

/*
 * Add openerp_synced field to sales_payment_transaction table
 *
 * Since sales_payment_transaction is not an entity, we only add a
 * column, we don't declare an attribute. No need to use Mage_Eav_Model_Entity_Setup()
 *
 */
$tableName = 'sales_payment_transaction';
if(!$db->tableColumnExists($this->getTable($tableName), 'openerp_synced'))
{
    $db->addColumn(
        $tableName,                         // Table
        'openerp_synced',                   // Field
        ' SMALLINT(1) unsigned DEFAULT 0'   // Definition (SQL)
    );
}

$installer->endSetup();


?>
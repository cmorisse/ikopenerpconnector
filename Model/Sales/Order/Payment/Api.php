<?php
/**
 * Allow to manage and sync Magento Payment Transactions with OpenERP
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Cyril MORISSE - @cmorisse
 *
 * TODO: add imported field to sales/order_payment_transaction
 * TODO: Implement search($filters)
 * TODO: Implement read($ids, $fields)
 * TODO: Implement searchRead($filters, $fields)
 * TODO: Implement write($ids, $values)
 */


class Inouk_IKOpenERPConnector_Model_Sales_Order_Payment_Api extends Mage_Api_Model_Resource_Abstract {

    /**
     * Return a list of id of transactions matching some given filters.
     *
     * @param  array $filters Array containing the criteria to apply for search.
     *                        Each array entry is itself an array containing a condition as key and
     *                        a value. See addFieldToFilter() definition
     *
     * @param int    $limit   integer Defines how much transactions we must return. If limit is specified transactions
     *                        are sorted by transaction_id.
     * @return array          All found transaction_id (increment_id).
     * @throws Mage_Api_Exception
     */
    public function search($filters, $limit=0) {

        $result = array();

        $collection = Mage::getModel("sales/order_payment_transaction")->getCollection();
        $collection->addAttributeToSelect('transaction_id');

        if (is_numeric($limit)) {
            if ($limit) {
                $collection->setPageSize($limit);
                $collection->setOrder('transaction_id', 'ASC');
            }
        } else {
            $this->_fault('invalid_limit_parameter');
        }

        if(isset($filters) && is_array($filters)) {
            foreach($filters as $field => $value) {
                $collection->addAttributeToFilter($field, $value);
            }
        } else {
            $this->_fault('invalid_filter_parameter');
        }

        foreach ($collection as $transaction) {
            $result[] =  $transaction['transaction_id'];
        }

        return $result;
    }

    /**
     * Return a list of id of transactions matching some given filters.
     *
     * @param array $ids    Array of all ids to read.
     * @param array $fields List of fields.
     *
     * @return array        Array containing all transactions.
     * @throws Mage_Api_Exception
     */
    public function read($ids, $fields='*') {

        $collection = Mage::getModel("sales/order_payment_transaction")->getCollection();
        $collection->setOrder('transaction_id', 'ASC');

        if($fields=='*') {
            $collection->addAttributeToSelect('*');
        } elseif (is_array($fields)) {
            foreach($fields as $field) {
                $collection->addAttributeToSelect($field);
            }
        } else
            $this->_fault('invalid_fields_parameter');

        if(isset($ids) && is_array($ids)) {
            $collection->addAttributeToFilter('transaction_id', array('in' => $ids));
        } else {
            $this->_fault('invalid_ids_parameter');
        }

        return $collection->getData();
    }

    public function searchRead($filters, $fields='*', $limit=0) {

        $collection = Mage::getModel("sales/order_payment_transaction")->getCollection();
        $collection->setOrder('transaction_id', 'ASC');

        if (is_numeric($limit)) {
            if ($limit) {
                $collection->setPageSize($limit);
            }
        } else {
            $this->_fault('invalid_limit_parameter');
        }

        if(isset($filters) && is_array($filters)) {
            foreach($filters as $field => $value) {
                $collection->addAttributeToFilter($field, $value);
            }
        } else {
            $this->_fault('invalid_filter_parameter');
        }

        if($fields=='*') {
            $collection->addAttributeToSelect('*');
        } elseif (is_array($fields)) {
            foreach($fields as $field) {
                $collection->addAttributeToSelect($field);
            }
        } else
            $this->_fault('invalid_fields_parameter');

        return $collection->getData();
    }

    /**
     * @param $ids
     * @param $value
     *
     * @throws Mage_Api_Exception
     */
    public function setOpenerpSynced($ids, $value)
    {

        if ( !(isset($ids)) ) {
            $this->_fault('invalid_parameter', "\$ids parameter is not defined.");
        } elseif( !is_array($ids) )
            $ids = array($ids);

        $resource = Mage::getModel('core/resource');
        $tableName = $resource->getTableName("sales/payment_transaction");

        $writeConnection = $resource->getConnection('core_write');

        $writeConnection->beginTransaction();
        try {

            $idsAsString = implode(',', $ids);
            $query = "UPDATE {$tableName} SET `openerp_synced` = {$value} WHERE `transaction_id` IN ({$idsAsString})";
            $writeConnection->query($query);
            $writeConnection->commit();
            return 1;  //

        } catch (Exception $e) {
            $writeConnection->rollBack();
            $this->_fault('transaction_update_failed', "transactions: {$idsAsString}, value: {$value}");
        }
    }

//    $model = Mage::getModel("sales/order_payment_transaction");
//    foreach ($ids as $id) {
//        $transaction = $model->load($id);
//        $a = $transaction->getOpenerpSynced();
//        $b = $transaction->setOpenerpSynced(5);
//        $model->save();
//        $c = $transaction->getOpenerpSynced();
//        // TODO: handle bad id case
//    }
//    return 1;

    public function setFlagForOrder($incrementId) {
        $_order = $this->_initOrder($incrementId);
        $_order->setImported(1);
        try {
            $_order->save();
            return true;
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
    }


    public function update($productId, $productData = array(), $store = null)
    {
        $product = $this->_getProduct($productId, $store);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }

        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }

        $this->_prepareDataForSave($product, $productData);

        try {
            if (is_array($errors = $product->validate())) {
                $this->_fault('data_invalid', implode("\n", $errors));
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }

        try {
            $product->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }

        return true;
    }


///*
//    /**
//     *
//     * Retrieve orders data based on the value of the flag 'imported'
//     * @param  array
//     * @return array
//     */
//    public function retrieveOrders($data) {
//
//        $result = array();
//        if(isset($data['imported'])) {
//
//            $collection = Mage::getModel("sales/order")->getCollection()
//                ->addAttributeToSelect('*')
//                ->addAttributeToFilter('imported', array('eq' => $data['imported']));
//
//            /* addAddressFields() is called only if version >= 1400 */
//            if(str_replace('.','',Mage::getVersion()) >= 1400) {
//                $collection->addAddressFields();
//            }
//
//            if(isset($data['limit'])) {
//                $collection->setPageSize($data['limit']);
//                $collection->setOrder('entity_id', 'ASC');
//            }
//
//            if(isset($data['filters']) && is_array($data['filters'])) {
//                $filters = $data['filters'];
//                foreach($filters as $field => $value) {
//                    $collection->addAttributeToFilter($field, $value);
//                }
//            }
//
//            foreach ($collection as $order) {
//                $tmp = $this->_getAttributes($order, 'order');
//
//                /* if version < 1400, billing and shipping information are added manually to order data */
//                if(str_replace('.','',Mage::getVersion()) < 1400) {
//                    $address_data = $this->_getAttributes($order->getShippingAddress(), 'order_address');
//                    if(!empty($address_data)) {
//                        $tmp['shipping_firstname'] = $address_data['firstname'];
//                        $tmp['shipping_lastname'] = $address_data['lastname'];
//                    }
//
//                    $address_data = $this->_getAttributes($order->getBillingAddress(), 'order_address');
//                    if(!empty($address_data)) {
//                        $tmp['billing_firstname'] = $address_data['firstname'];
//                        $tmp['billing_lastname'] = $address_data['lastname'];
//                    }
//                }
//
//                $result[] = $tmp;
//            }
//            return $result;
//        }else{
//            $this->_fault('data_invalid', "Error, the attribut 'imported' need to be specified");
//        }
//    }
//
//    public function setFlagForOrder($incrementId) {
//        $_order = $this->_initOrder($incrementId);
//        $_order->setImported(1);
//        try {
//            $_order->save();
//            return true;
//        } catch (Mage_Core_Exception $e) {
//            $this->_fault('data_invalid', $e->getMessage());
//        }
//    }
//
//    /* Retrieve increment_id of the child order */
//    public function getOrderChild($incrementId) {
//
//        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
//        /**
//         * Check order existing
//         */
//        if (!$order->getId()) {
//            $this->_fault('order_not_exists');
//        }
//
//        if($order->getRelationChildId()) {
//            return $order->getRelationChildRealId();
//        }else{
//            return false;
//        }
//    }
//
//    /* Retrieve increment_id of the parent order */
//    public function getOrderParent($incrementId) {
//
//        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
//        /**
//         * Check order existing
//         */
//        if (!$order->getId()) {
//            $this->_fault('order_not_exists');
//        }
//
//        if($order->getRelationParentId()) {
//            return $order->getRelationParentRealId();
//        }else{
//            return false;
//        }
//    }
//
//    /* Retrieve invoices increment ids of the order */
//    public function getInvoiceIds($incrementId) {
//        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
//        /**
//         * Check order existing
//         */
//        if (!$order->getId()) {
//            $this->_fault('order_not_exists');
//        }
//        $res = array();
//        foreach($order->getInvoiceCollection() as $invoice){
//            array_push($res, $invoice->getIncrementId());
//        };
//        return $res;
//    }
//
//    /* Retrieve shipment increment ids of the order */
//    public function getShipmentIds($incrementId) {
//        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
//        /**
//         * Check order existing
//         */
//        if (!$order->getId()) {
//            $this->_fault('order_not_exists');
//        }
//        $res = array();
//        foreach($order->getShipmentsCollection() as $shipping){
//            array_push($res, $shipping->getIncrementId());
//        };
//        return $res;
//    }
//
//    /**
//     * Renovi les transactions d'une commande
//     * @var string $ordier_id
//     * @return array
//     */
//    public function transactions($order_id)
//    {
//        $result = array();
//        $order = Mage::getModel('sales/order')->load($order_id);
//        if (!$order->getId())
//        {
//            $this->_fault('not_exists');
//        }
//
//
//        $collection = Mage::getResourceModel('sales/order_payment_transaction_collection');
//        $collection->addOrderIdFilter($order->getId());
//
//        return $collection->getData();
//    }*/
}

?>